package com.example.homework6

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        lateinit var dbHandler: DB
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dbHandler = DB(this, null, null, 1)

        view()

        buttonAddItem.setOnClickListener(){
            val intent = Intent(this, AddItem::class.java)
            startActivity(intent)
        }
    }

    @SuppressLint("")
    private fun view(){
        val itemsList = dbHandler.getItems(this)

    }

    override fun onResume() {
        view()
        super.onResume()
    }
}
