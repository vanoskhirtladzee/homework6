package com.example.homework6

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_add_item.*

class AddItem : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_item)


        saveButton.setOnClickListener(){
            if (editTitle.text.isEmpty()){
                Toast.makeText(this, "Enter item title", Toast.LENGTH_SHORT).show()
            } else{
                val item = Models()
                item.itemTitle = editTitle.text.toString()
                if (editDescription.text.isEmpty())
                    item.itemDesc = "" else
                    item.itemDesc = editDescription.text.toString()
                MainActivity.dbHandler.addItem(this, item)
            }

        }
    }
}
