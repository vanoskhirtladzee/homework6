package com.example.homework6

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.items.view.*

class Adapters(private val context: Context, private val items : MutableList<Models>): RecyclerView.Adapter<Adapters.MyViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Adapters.MyViewHolder {
        val a = LayoutInflater.from(parent.context).inflate(R.layout.items, parent, false )
        return RecyclerView.ViewHolder(a)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: Adapters, position: Int) {

    }
    class MyViewHolder(itemView:View): RecyclerView.ViewHolder(itemView){
        val itemTitle = itemView.itemTitle
        val itemDescription = itemView.itemDescription
        val btnUpdate = itemView.buttonUpdate
        val btnDelete = itemView.buttonDelete


    }
}