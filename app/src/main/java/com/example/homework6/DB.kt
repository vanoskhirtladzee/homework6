package com.example.homework6

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import android.widget.Toast
import java.lang.Exception

class DB(context : Context, name : String?, factory: SQLiteDatabase.CursorFactory?, version : Int) :
    SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {


    companion object {
        private val DATABASE_NAME = "Homework6.db"
        private val DATABASE_VERSION = 1

        val ITEM_TABLE_NAME = "Homework6"
        val ITEM_COLUMN_ID = "ID"
        val ITEM_COLUMN_TITLE = "Title"
        val ITEM_COLUMN_DESCRIPTION = "Description"
    }
    override fun onCreate(db: SQLiteDatabase?) {
        val CREATE_TASK_TABLE = ("CREATE TABLE $ITEM_TABLE_NAME (" +
                "$ITEM_COLUMN_ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                "$ITEM_COLUMN_TITLE TEXT," +
                "$ITEM_COLUMN_DESCRIPTION TEXT)")
        db?.execSQL(CREATE_TASK_TABLE)

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun getItems(ctx : Context) : ArrayList<Models>{
        val query = "Select * From $ITEM_TABLE_NAME"
        val db = this.readableDatabase
        val cursor = db.rawQuery(query, null)
        val items = ArrayList<Models>()

        if (cursor.count == 0)
            Toast.makeText(ctx, "No Items found", Toast.LENGTH_SHORT).show()

        else{
            cursor.moveToFirst()
            while (!cursor.isAfterLast){
                val item = Models()
                item.itemID = cursor.getInt(cursor.getColumnIndex(ITEM_COLUMN_ID))
                item.itemTitle = cursor.getString(cursor.getColumnIndex(ITEM_COLUMN_TITLE))
                item.itemDesc = cursor.getString(cursor.getColumnIndex(ITEM_COLUMN_DESCRIPTION))
                items.add(item)
                cursor.moveToNext()
            }

        }

        cursor.close()
        db.close()
        return items
    }

    fun addItem(ctx: Context, item : Models){
        val values = ContentValues()
        values.put(ITEM_COLUMN_TITLE, item.itemTitle)
        values.put(ITEM_COLUMN_DESCRIPTION, item.itemDesc)
        val db = this.writableDatabase
        db.insert(ITEM_TABLE_NAME, null, values)
        db.close()

    }
    fun deleteItem(itemID:Int) : Boolean {
        val query = "Delete From $ITEM_TABLE_NAME where $ITEM_COLUMN_ID = $itemID"
        val db = this.writableDatabase
        var result : Boolean = false
        try{
            val cursor = db.execSQL(query)
            result = true
        } catch(e : Exception) {
            Log.d(ContentValues.TAG, "Error")
        }
        db.close()
        return result

    }

    fun updateItem(id: Int, itemName: String, itemDesc: String  ) : Boolean {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        var result : Boolean = false
        contentValues.put(ITEM_COLUMN_TITLE, itemName)
        contentValues.put(ITEM_COLUMN_DESCRIPTION, itemDesc)
        try {
            db.update(ITEM_TABLE_NAME, contentValues, "$ITEM_COLUMN_ID = ?")
            result = true
        } catch (e: Exception){
            result = false
        }
        return result
    }
}